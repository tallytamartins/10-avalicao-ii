import React, { useEffect, useState } from 'react'
import { productsList } from '../dates/productsList'
import { Product } from '../components/Product/Product'
import '../styles/index.css'

const IndexPage = () => {
  const [cart, setCart] = useState<Product[]>([])
  const [cartOpen, setCartOpen] = useState(false)
  const [total, setTotal] = useState(0)
  const [discount, setDiscount] = useState(0)

  useEffect(() => {
    const updateTotalCart = () => {
      const priceItens = cart.map(item => item.quantity * item.price)
      const priceTotalItens = priceItens.reduce((a, b) => a + b, 0)

      if (priceTotalItens >= 100) {
        setDiscount((priceTotalItens * 10) / 100)
      }
      setTotal(priceTotalItens)
    }
    updateTotalCart()
  }, [cart])

  const addInCart = async (product: Product) => {
    if (cart.findIndex(item => item.name === product.name) === -1) {
      setCart([...cart, product])
    } else {
      const cartUpdate = cart.map(item => {
        if (item.name === product.name) {
          return { ...item, quantity: item.quantity + 1 }
        }
        return item
      })

      setCart(cartUpdate)
    }
  }

  const updateQuantity = (product: Product, method: string) => {
    if (product.quantity === 1 && method !== 'sum') {
      return removeFromCart(product)
    }

    const cartUpdate = cart.map(item => {
      if (item.name === product.name) {
        return {
          ...item,
          quantity: method === 'sum' ? item.quantity + 1 : item.quantity - 1
        }
      }
      return item
    })

    setCart(cartUpdate)
  }

  const removeFromCart = (product: Product) => {
    const cartUpdate = cart.filter(item => {
      if (item.name === product.name) {
        return
      }
      return item
    })

    setCart(cartUpdate)
  }

  return (
    <main>
      <h1>Lista de Produtos</h1>
      <h3>A cada R$100 em compras, ganhe 10% de desconto</h3>

      <button className="button-cart" onClick={() => setCartOpen(!cartOpen)}>
        {cartOpen ? 'x' : `Meu carrinho (${cart.length})`}
      </button>

      <div className="products-container">
        {productsList.map((product, index) => (
          <Product
            product={product}
            key={index}
            namebutton={'Adicionar ao carrinho'}
            addInCart={addInCart}
          />
        ))}
      </div>

      {cartOpen && (
        <div className="cart-container">
          {cart.map((product, index) => (
            <Product
              product={product}
              key={index}
              namebutton="Remover"
              updateQuantity={updateQuantity}
              removeFromCart={removeFromCart}
            />
          ))}
          <div>
            <h2>Valor total:{total}</h2>
            <h2>Desconto:{discount}</h2>
            <h2>Valor final:{total - discount}</h2>
          </div>
        </div>
      )}
    </main>
  )
}

export default IndexPage
