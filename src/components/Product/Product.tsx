import React, { useState } from 'react'

interface ProductProps {
  product: Product
  namebutton: string
  addInCart?: (product: Product) => void
  updateQuantity?: (product: Product, method: string) => void
  removeFromCart?: (product: Product) => void
}

export const Product: React.FC<ProductProps> = ({
  product,
  namebutton,
  addInCart,
  updateQuantity,
  removeFromCart
}) => {
  return (
    <div className="product-container">
      <img src={product.image} alt={product.name} />
      <h2>{product.name}</h2>
      <p>
        Estágio de Evolução: <strong>{product.evolutionStage}</strong>
      </p>
      <p>
        Preço: <strong>{product.price}</strong>
      </p>
      {namebutton === 'Remover' && <h3>Quantidade: {product.quantity}</h3>}
      {namebutton === 'Remover' && (
        <h3>
          Valor total:{' '}
          {updateQuantity && <span>{product.quantity * product.price}</span>}
        </h3>
      )}
      {updateQuantity && (
        <button
          className="buttonSubtract"
          onClick={() => updateQuantity && updateQuantity(product, 'subtract')}
        >
          -
        </button>
      )}
      <button
        className="buttonProducts"
        onClick={() => {
          if (namebutton === 'Adicionar ao carrinho') {
            addInCart && addInCart(product)
          } else {
            removeFromCart && removeFromCart(product)
          }
        }}
      >
        {namebutton}
      </button>
      {updateQuantity && (
        <button
          className="buttonSum"
          onClick={() => updateQuantity(product, 'sum')}
        >
          +
        </button>
      )}
    </div>
  )
}
