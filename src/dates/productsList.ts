export const productsList: Product[] = [
  {
    image: 'https://assets.pokemon.com/assets/cms2/img/pokedex/full/001.png',
    name: 'Bulbasaur',
    evolutionStage: 1,
    price: 10,
    quantity: 1
  },
  {
    image: 'https://assets.pokemon.com/assets/cms2/img/pokedex/full/002.png',
    name: 'Ivysaur',
    evolutionStage: 2,
    price: 20,
    quantity: 1
  },
  {
    name: 'Venusaur',
    image: 'https://assets.pokemon.com/assets/cms2/img/pokedex/full/003.png',
    price: 30,
    evolutionStage: 3,
    quantity: 1
  },
  {
    name: 'Charmander',
    image: 'https://assets.pokemon.com/assets/cms2/img/pokedex/full/004.png',
    price: 10,
    evolutionStage: 1,
    quantity: 1
  },
  {
    name: 'Charmeleon',
    image: 'https://assets.pokemon.com/assets/cms2/img/pokedex/full/005.png',
    price: 20,
    evolutionStage: 2,
    quantity: 1
  },
  {
    name: 'Charizard',
    image: 'https://assets.pokemon.com/assets/cms2/img/pokedex/full/006.png',
    price: 30,
    evolutionStage: 3,
    quantity: 1
  },
  {
    name: 'Squirtle',
    image: 'https://assets.pokemon.com/assets/cms2/img/pokedex/full/007.png',
    price: 10,
    evolutionStage: 1,
    quantity: 1
  },
  {
    name: 'Wartortle',
    image: 'https://assets.pokemon.com/assets/cms2/img/pokedex/full/008.png',
    price: 20,
    evolutionStage: 2,
    quantity: 1
  },
  {
    name: 'Blastoise',
    image: 'https://assets.pokemon.com/assets/cms2/img/pokedex/full/009.png',
    price: 30,
    evolutionStage: 3,
    quantity: 1
  },
  {
    name: 'Caterpie',
    image: 'https://assets.pokemon.com/assets/cms2/img/pokedex/full/010.png',
    price: 10,
    evolutionStage: 1,
    quantity: 1
  },
  {
    name: 'Metapod',
    image: 'https://assets.pokemon.com/assets/cms2/img/pokedex/full/011.png',
    price: 20,
    evolutionStage: 2,
    quantity: 1
  },
  {
    name: 'Butterfree',
    image: 'https://assets.pokemon.com/assets/cms2/img/pokedex/full/012.png',
    price: 30,
    evolutionStage: 3,
    quantity: 1
  },
  {
    name: 'Weedle',
    image: 'https://assets.pokemon.com/assets/cms2/img/pokedex/full/013.png',
    price: 10,
    evolutionStage: 1,
    quantity: 1
  },
  {
    name: 'Kakuna',
    image: 'https://assets.pokemon.com/assets/cms2/img/pokedex/full/014.png',
    price: 20,
    evolutionStage: 2,
    quantity: 1
  },
  {
    name: 'Beedrill',
    image: 'https://assets.pokemon.com/assets/cms2/img/pokedex/full/015.png',
    price: 30,
    evolutionStage: 3,
    quantity: 1
  }
]
