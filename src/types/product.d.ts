interface Product {
  name: string
  price: number
  image: string
  evolutionStage: number
  quantity: number
}